import random
from flask import Flask, request, render_template

app = Flask(__name__)
app.config['DEBUG'] = True

@app.route("/main")
def main():
    return render_template("/main.html")
    
@app.route("/create")

def random_quotes_plus_movies():
    movie = request.form["movie"]
    movie2 = request.form["movie2"]
    movie_quote_list = ["Khaaan","Heres Johnny!","Luke, I am your father"]
    movie_list = ["Star Trek", "The Shining", "Star Wars"]
    random_movie_quote = random.choice(movie_quote_list)
    random_movie = random.choice(movie_list)
    return render_template("main.html", movie=movie, movie2=movie2)
app.run()
